package com.barro.s03activity;

import java.util.Scanner;

public class ComputeFactorial {
    public static void main(String[] args){
        Scanner input= new Scanner(System.in);

        int num = 1;
        int x = 1;
        int factorial = 1;
        System.out.println("Enter a number: ");

        try{
            num = input.nextInt();

        } catch(Exception err){
                System.out.println("Input is not a number");
        }

       while(num >= x){
           factorial = factorial*x;
           x++;
     }
       if(num < 0){
            System.out.println("Input must be a positive integer");
        }else{
            System.out.println(factorial);
        }

    }
}
